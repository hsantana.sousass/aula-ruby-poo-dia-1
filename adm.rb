require './bd.rb'
require './funcionario.rb'
require './dependente.rb'
require './departamento.rb'
require './projeto.rb'

class Adm < Funcionario
    def self.cadastra_funcionario hash
        Bd.armazena_funcionarios(Funcionario.new(hash))
        puts "Cadastrado!"
    end

    def self.cadastra_dependente hash
        Bd.armazena_dependente(Dependente.new(hash))
        puts "Cadastrado!"
    end

    def self.cadastra_departamento hash
        Bd.armazena_departamento(Departamento.new(hash))
        puts "Cadastrado!"
    end

    def self.cadastra_projeto hash
        Bd.armazena_projeto(Projeto.new(hash))
        puts "Cadastrado!"
    end

    def self.adiciona_gerente_departamento array
        departamento = Bd.busca_departamento(array[0])
        funcionario = Bd.busca_funcionario(array[1])
        if departamento[0] and funcionario[0]
            departamento[0].gerente = funcionario[0].nome_completo
            puts "Gerente adicionado!"
        else
           puts "#{departamento[1]}  ou #{funcionario[1]}"
        end
    end

    def self.mostra_funcionario matricula
        Bd.busca_funcionario(matricula)
    end

    def self.mostra_departamento codigo
        Bd.busca_departamento(codigo)
    end

end