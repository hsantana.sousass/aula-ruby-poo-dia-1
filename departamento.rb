class Departamento
    attr_accessor :nome, :gerente, :codigo
    def initialize params
        @@funcionarios_departamento = []
        @nome = params[:nome]
        @gerente = nil
        @codigo = Departamento.gera_codigo
    end

    def self.gera_codigo
        bd_departamentos = Bd.departamentos
        begin
            (bd_departamentos[(bd_departamentos.length) - 1].codigo) + 1   
        rescue
            1
        end
    end
    
    def adiciona_funcionario matricula
        funci = Bd.busca_funcionario
        if funci[0]
            @@funcionarios_departamento= funci[0]   
        end

        funci[1]
    end

    def calcula_gastos
        total = 0
        @@funcionarios_departamento.each do |funcionario|
            total += funcionario.salario
        end
    end

    def numero_de_funcionarios
        @@funcionarios_departamento.length
    end

    def listar_funcionarios
        if @@funcionarios_departamento.length > 0
            @@funcionarios_departamento.each do |funcionario|
                puts funcionario.nome_completo
            end
        end
    end
end
