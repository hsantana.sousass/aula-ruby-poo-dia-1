require './bd.rb'

class Funcionario
    attr_accessor :primeiro_nome, :segundo_nome, :idade, :salario, :endereco, :dependentes, :setor, :matricula
    def initialize params
        @@primeiro_nome = params[:primeiro_nome].downcase
        @@segundo_nome = params[:segundo_nome].downcase
        @idade = params[:idade]
        @salario = params[:salario]
        @endereco = params[:endereco]
        @@dependentes = []
        @setor = params[:setor]
        @matricula = Funcionario.gerador_matricula
    end

    def self.gerador_matricula
        bd_funcionarios = Bd.funcionarios
        begin
            (bd_funcionarios[(bd_funcionarios.length) - 1].matricula) + 1   
        rescue
            1
        end
    end

    def dependentes
        @@dependentes
    end

    def nome_completo     
        "#{@@primeiro_nome} #{@@segundo_nome}"
    end

    def adicionar_dependente codigo
        depen = Bd.busca_dependente
        if depen[0]
            @@dependentes.push(funci[0])  
        else
            depen[1]  
        end  
    end
end