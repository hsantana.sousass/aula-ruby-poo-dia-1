require "./scripts.rb"
require "./adm"

def tela_inicial
    puts "---------------------------------> QUADRO DE FUNCIONÁRIOS <---------------------------------------"
    puts " 
->:Escolha a área que deseja realizar a ação:<
-> [1] Funcionário
        - Cadastrar funcionário
        - Cadastrar dependente
        - Mostrar funcionário
-> [2] Departamento
        - Cadastrar departamento
        - Adicionar gerente em um departamento
        - Informações sobre um departamento
-> [3] Projeto
        - Cadastrar projeto
-----------------------------------------------------------------------------------------------------
        "
end

def tela_informacoes_funcionario
    funcionario = Adm.mostra_funcionario(pega_matricula)
    if not funcionario[0]
        puts funcionario[1]
    else
        
        puts"
        ====================================
        Nome: #{funcionario[0].nome_completo}
        idade: #{funcionario[0].idade}
        salário: #{funcionario[0].salario}
        Setor: #{funcionario[0].setor}
        Endereço: #{funcionario[0].endereco}
        ====================================
        "
    end
end

def tela_funcionario
    puts"
-> [1] - Cadastrar funcionário
-> [2] - Cadastrar dependente
-> [3] - Mostrar funcionário
        "
    puts "Digite o número de uma das opções:"
    comando = gets.chomp
    if comando == "1"
        Adm.cadastra_funcionario(cadastrar_funcionario)  
    elsif comando == "2"
        Adm.cadastra_dependente(cadastrar_dependente)
    elsif comando == "3"
        tela_informacoes_funcionario 
    else 
        puts "Comando inválido"
    end
end

def tela_informacoes_departamento
    departamento = Adm.mostra_departamento(pega_codigo)
    if not departamento[0]
        puts departamento[1]
    else 
        puts"
        ====================================
        Nome: #{departamento[0].nome}
        gerente: #{departamento[0].gerente}
        funcionarios == numero:#{departamento[0].numero_de_funcionarios}
        #{departamento[0].listar_funcionarios}
        ====================================
        "
    end
end

def tela_departamento
    puts"
-> [1] - Cadastrar departamento
-> [2] - Adicionar gerente em um departamento
-> [3] - Informações sobre um departamento
        "
    puts "Digite o número de uma das opções:"
    comando = gets.chomp
    if comando == "1"
        Adm.cadastra_departamento(cadastrar_departamento)
        puts  
    elsif comando == "2"
        Adm.adiciona_gerente_departamento(cadastrar_gerente_no_departamento) 
        puts
    elsif comando == "3"
       tela_informacoes_departamento
       puts
    else 
        puts "Comando inválido\n"
    end
end

def tela_projeto
    cadastrar_projeto
end

def main 
    estado = true
    while estado
        tela_inicial
        puts "Digite o número de uma das opções:"
        comando = gets.chomp
        if comando == "1"
            tela_funcionario
            puts   
        elsif comando == "2"
            tela_departamento
            puts  
        elsif comando == "3"
            tela_projeto
            puts  
        elsif comando == "4"
            estado = false 
        else 
            puts "Comando inválido\n"
        end
    end
end