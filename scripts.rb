# sem validação, apenas para teste das classes

def cadastrar_funcionario
    puts "Primeiro nome:"
    primeiro_nome = gets.chomp
    puts "Segundo nome:"
    segundo_nome = gets.chomp
    puts "idade:"
    idade = gets.chomp
    puts "Salário:"
    salario = gets.chomp
    puts "Endereço:"
    endereco = gets.chomp
    puts "Setor:"
    setor = gets.chomp

    {
        :primeiro_nome => primeiro_nome,
        :segundo_nome => segundo_nome,
        :idade => idade.to_i,
        :salario => salario.to_f,
        :endereco => endereco,
        :setor => setor
    }
end


def cadastrar_dependente
    puts "Nome:"
    nome = gets.chomp
    puts "idade:"
    idade = gets.chomp
    puts "Parentesco:"
    parentesco = gets.chomp

    {
        :nome => nome,
        :idade => idade.to_i,
        :parentesco => parentesco
    }
end


def cadastrar_departamento
    puts "nome:"
    nome = gets.chomp

    {
        :nome => nome
    }
end

def cadastrar_projeto
    puts "Nome:"
    nome = gets.chomp
    puts "Cliente:"
    cliente = gets.chomp
    puts "Fundos:"
    fundos = gets.chomp

    {
        :nome => nome, 
        :cliente => cliente,
        :fundos => fundos.to_f,
    }
end

def cadastrar_gerente_no_departamento
    puts "Código do departamento:"
    codigo_departamento = gets.chomp.to_i
    puts "Matrícula do funcionário:"
    gerente_departamento = gets.chomp.to_i

    [codigo_departamento, gerente_departamento]
end

def pega_matricula
    puts "Matrícula:"
    matricula = gets.chomp.to_i
end

def pega_codigo
    puts "Código:"
    codigo = gets.chomp.to_i
end
