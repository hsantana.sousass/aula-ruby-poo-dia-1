class Dependente
    attr_accessor :nome, :idade, :parentesco, :codigo
    def initialize params
        @nome = params[:nome]
        @idade = params[:idade]
        @parentesco = params[:parentesco]
        @codigo = Dependente.gera_codigo
    end

    def self.gera_codigo
        bd_dependentes = Bd.dependentes
        begin
            (bd_dependentes[(bd_dependentes.length) - 1].codigo) + 1   
        rescue
            1
        end
    end
end