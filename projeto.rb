class Projeto
    attr_accessor :nome, :cliente, :fundos
    def initialize params
        @nome = params[:nome]
        @cliente = params[:cliente]
        @fundos = params[:fundos]
        @@departamento = nil
        @id = gera_id
    end

    def adicionar_departamento codigo
        depart = Bd.busca_departamento codigo
        if depart[0]
            if depart[0].calcula_gastos < depart[0].fundos
                @@departamento = depart[0]  
            end
            
            "Fundos insuficiente"
        end

        depart[1]
    end
end