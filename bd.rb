class Bd
    @@funcionarios = []
    @@dependentes = []
    @@departamentos = []
    @@projetos = []

    def self.busca_funcionario matricula
        @@funcionarios.each do |funcionario|
            if funcionario.matricula == matricula
                return [funcionario]
            end
        end

        [false, "Matricula #{matricula} não encontrada"]
    end

    def self.busca_dependente codigo
        @@dependentes.each do |dependente|
            if dependente.codigo == codigo
               return [dependente]
            end
        end

        [false, "Código #{codigo} não encontrado"]
    end

    def self.busca_departamento codigo
        @@departamentos.each do |departamento|
            if departamento.codigo == codigo
                return [departamento]
            end
        end

        [false, "Código #{codigo} não encontrado"]
    end

    def self.busca_projeto id
        @@projetos.each do |projeto|
            if projeto.id == id
                return [projeto]
            end
        end

        [false, "Id #{id} não encontrado"]
    end

    def self.armazena_funcionarios dado
        @@funcionarios.push(dado)
    end

    def self.armazena_dependente dado
        @@dependentes.push(dado)
    end

    def self.armazena_departamento dado
        @@departamentos.push(dado)
    end

    def self.armazena_projetos dado
        @@projetos.push(dado)
    end

    def self.funcionarios
        @@funcionarios
    end

    def self.dependentes
        @@dependentes
    end

    def self.departamentos
        @@departamentos
    end

    def self.projetos
        @@projetos
    end
end